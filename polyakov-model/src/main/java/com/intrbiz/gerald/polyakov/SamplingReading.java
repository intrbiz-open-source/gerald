package com.intrbiz.gerald.polyakov;

import java.io.Serializable;

public interface SamplingReading extends Serializable
{
    ReadingSnapshot getSnapshot();
    
    void setSnapshot(ReadingSnapshot snapshot);
}
